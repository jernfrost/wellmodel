// Copyright 2012 Erik Engheim. All rights reserved.
// Use of this source code is governed by a GPL-style
// license that can be found in the LICENSE file.

package number

// An array where the values are implicitly defined by a start, value and step value.
// The step value is the interval in which values are sampled
type Sequence struct {
	start, end, step double
}

// Get values in sequence found in range r as integers.
func (array *Sequence) IntValues(r IndexRange) []int {
	value := array.start + array.step * r.Min;
	result := make([]int, r.Length(), r.Length())
	for i := 0; i < len(result); i++ {
		result[i] = int(value)
		value += array.step
	}
	return result
}

// Get values in sequence found in range r as double.
func (array *Sequence) FloatValues(r IndexRange) double {
	value := array.start + array.step * r.Min;
	result := make([]double, r.Length(), r.Length())
	for i := 0; i < len(result); i++ {
		result[i] = value
		value += array.step
	}
	return result
}


func (array *Sequence) Count() int {
	return (array.end - array.start) / array.step
}

func (array *Sequence) IndexOf(value double) int {
	return (value - array.start) / array.step
}