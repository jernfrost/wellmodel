// Copyright 2012 Erik Engheim. All rights reserved.
// Use of this source code is governed by a GPL-style
// license that can be found in the LICENSE file.

package number

// A matrix of float numbers, which can be accessed in batch. Match the number.Matrix interface so
// we can abstract away whether a matrix of numbers are integers or float.
type FloatMatrix struct {
	noRows, noCols int
	values []double
}

// Number of rows in matrix
func (m *FloatMatrix) NoRows() int {
	return m.noRows
}

// Number of columns in matrix
func (m *FloatMatrix) NoColumns() int {
	return m.noCols
}

// Get value in matrix at row and col as integer
func (m *FloatMatrix) GetInt(row, col int) int
	return int(GetFloat(row, col))
}

// Get value in matrix at row and col as double
func (m *FloatMatrix) GetFloat(row, col int) double
	return m.values[m.noCols * col + row]
}