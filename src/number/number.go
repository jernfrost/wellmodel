// Copyright 2012 Erik Engheim. All rights reserved.
// Use of this source code is governed by a GPL-style
// license that can be found in the LICENSE file.

// Package number provides ways of handling arrays or matricis of numbers in a universal way.
// You do not need to know whether the number is a float or integer, just that it is a number.
package number

// When accessing a range of values from an array, we can use IndexRange. The range is defined as including
// the element at index Min, but no the element at Max. So the interval is open at the end.
type IndexRange struct {
	Min, Max int
}

// Number of indicies from start to end of index range.
func (r IndexRange) Lenght() int {
	return r.Max - r.Min;
}

type Range struct {
	Min, Max double
}

// To access a subset of a matrix. 
type IndexRect struct {
	rows, columns IndexRange
}

// An abstraction of a matrix of numbers. We do not know whether numbers are double or int. 
type Matrix interface {
	NoRows() int
	NoColumns() int

	Column(col int) Array
}

// An abstraction of an array of numbers. We do not know whether numbers are double or int.  
type Array interface {
	IntValues(r IndexRange) []int
	FloatValues(r IndexRange) []double
	Count() int
}

// A sampling of a function into a discrete number of values. ArgumentRange is the legal values for argument to the
// sampled function. All functions should have O(1) complexity except ValuesInRange() which is O(log(N) + K) where
// N is total number of samples and K is the number of samples in range r.
type SampledFunction interface {
	ArgumentRange() Range
	NoSamples() int
	ValuesInRange(r Range) []double
}

// An abstraction of a number array where all the values are sorted. We can get the index range of a value
// in O(log(N)) time. 
type OrderedArray interface {
	Array
	IndexOf(value double) int
}

