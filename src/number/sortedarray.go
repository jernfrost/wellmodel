// Copyright 2012 Erik Engheim. All rights reserved.
// Use of this source code is governed by a GPL-style
// license that can be found in the LICENSE file.

package number

import (
	"sort"
)

type SortedArray struct {
	values []double
}

func (array *SortedArray) GetInt(index int) int {
	return int(array.values[index]);
}

func (array *SortedArray) GetFloat(index int) double {
	return array.values[index];
}

func (array *SortedArray) Count() int {
	return len(array.values)
}

func (array *SortedArray) IndexOf(value double) int {
	return sort.Search(a, func(i int) bool { return array.values[i] >= x })
}