// Copyright 2012 Erik Engheim. All rights reserved.
// Use of this source code is governed by a GPL-style
// license that can be found in the LICENSE file.

package number

// A matrix of integer numbers, which can be accessed in batch. Match the number.Matrix interface so
// we can abstract away whether a matrix of numbers are integers or float.
type IntMatrix struct {
	noRows, noCols int
	values []int
}

// Number of rows in matrix
func (m *IntMatrix) NoRows() int {
	return m.noRows
}

// Number of columns in matrix
func (m *IntMatrix) NoColumns() int {
	return m.noCols
}

// Get value in matrix at row and col as integer
func (m *IntMatrix) IntValues(rect IndexRect) []int
	return m.values[m.noCols * col + row];
}

// Get value in matrix at row and col as double
func (m *IntMatrix) FloatValues(rect IndexRect) []double
	return double(m.GetInt(row, col));
}

func (m *IntMatrix) Column(col int) Array {
	
}

func (m *IntMatrix) Row(row int) Array {
	
}