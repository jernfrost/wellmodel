// Copyright 2012 Erik Engheim. All rights reserved.
// Use of this source code is governed by a GPL-style
// license that can be found in the LICENSE file.

package well

import (
	"dynamiclog"
	"number"
)

type LogData struct {
	log Log
	mdlog number.OrderedArray
}

func (logdata *WellData) ArgumentRange() Range {
	return mdLog
}

func (logdata *WellData) NoSamples() int {
	return logdata.log.NoRows()
}

func (logdata *WellData) ValuesInRange(r Range) []double {
	// TODO: make sure the borders are correct. Min should be index for equal or larger values
	// while for max it should be index of smaller value.
	min := logdata.mdlog.IndexOf(r.Min)
	max := logdata.mdlog.IndexOf(r.Max)  
	return logdata.log.FloatValues(IndexRange{min, max})
}