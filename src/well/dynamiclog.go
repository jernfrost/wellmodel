// Copyright 2012 Erik Engheim. All rights reserved.
// Use of this source code is governed by a GPL-style
// license that can be found in the LICENSE file.

package well

import (
	"number"
)

// A well log which can grow and shrink in size.
type DynamicLog struct {
	name string
	data number.Matrix
}

func (log *DynamicLog) Name() string {
	return log.name
}

func (log *DynamicLog) NoColumns() int {
	return log.data.NoCols()
}

func (log *DynamicLog) Data() number.Array {
	return log.data.
}

func (log *DynamicLog) IsImageLog() bool {
	return log.data.NoCols() > 1
}


