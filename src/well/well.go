// Copyright 2012 Erik Engheim. All rights reserved.
// Use of this source code is governed by a GPL-style
// license that can be found in the LICENSE file.

// Package well provides data structures and algorithms related to oil wells.
// Wells are understood as oil platforms as well as the land based ones where
// a hole has been drilled deep to extract oil. The package also contains all
// data structures related to wells such as representations of the well trajectories, 
// data logged from measuring instruments pulled through the bore hole etc.
package well


type Log interface {
	Name() string
	NoColumns() int
	Data() number.Array
	IsImageLog() bool
	IsEditable() bool
	SetEditable(editable bool)
	IsDirty() bool
	SetDirty(dirty bool)
}