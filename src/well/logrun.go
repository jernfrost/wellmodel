// Copyright 2012 Erik Engheim. All rights reserved.
// Use of this source code is governed by a GPL-style
// license that can be found in the LICENSE file.

package well

import (
	"dynamiclog"
	"number"
)

// Logrun where log values are sampled at regular interval
const REGULAR = 1

// Log values are sampled at irregular intervals
const IRREGULAR = 2

// Log values are sampled for a whole interval. The intervals are not regular.
const INTERVAL = 3

// When a measuring instrument is pulled through a well trajectory at one use different instruments to capture
// properties of the surrounding rock on regular or irregular intervals then that constitutes a logrun. A logrun
// consists of multiple logs. Each type of measuring instrument produce one well log. All the logs of the logrun 
// share the same Measured depth log (MD log). That is because all logs get their values sampled at the same positions
// as the logging instruments are pulled up the well trajectory. 
//
// There are three types of Log Runs: Regular, Irregular and Interval. All of them can have both
// Discrete and Continous logs. The main difference is between how the MD log is defined.
type LogRun struct {
	mdLog number.OrderedArray
	continousLogs map[string] DynamicLog
	discreteLogs  map[string] DynamicLog
}

// Creates a log run where every log value has been measured at a regular interval. Thus
// an MD log can be described entierly by top md, bottom md and a step value. The step value
// indicates the size of the interval between measurements
func CreateRegularLogRun(topmd, bottommd, step double) *LogRun {
	
}

// Creates a log run where log values have been measured at irregular intervals. This requires
// us to store the MD (measured depth) value for each log value. 
func CreateIregularLogRun(mds []double) *LogRun {
	
}

// Similar to irregular log runs, but pairs of md values now represent intervals. So md[i] to md[i+1] is an 
// interval where the log value is log[i]. In the cases where we have en empty interval log[i] == Undefined.
func CreateIntervalLogRun(mds []double) *LogRun {
	
}

// Create a continous log with given name. Continous logs usually represent physical properties of surrounding rock
// such a restitivity towards electric current, gamma ray emmition etc.
func (logrun *LogRun) AddContinuousLog(logname string) Log {
	
}

func (logrun *LogRun) AddImageLog(logname string, columns int) Log {
	
}

// Create a discrete log. Usually logs which are derives from continous logs. Usually indicate facies of the
// surrounding rock: sand stone, shale etc.
func (logrun *LogRun) AddDiscreteLog(logname string) Log {
	
}

func (logrun *LogRun) DiscreteLogNames() []string {
	names := make([]string, 0, 20)
	for key, _ := range logrun.discreteLogs {
		names = append(names, key)
	}
	return names
}

func (logrun *LogRun) ContinousLogNames() []string {
	names := make([]string, 0, 20)
	for key, _ := range logrun.discreteLogs {
		names = append(names, key)
	}
	return names	
}

func (logrun *LogRun) LogNames() []string {
	names := logrun.DiscreteLogNames()
	return append(names, logrun.ContinousLogNames()...)
}

func (logrun *LogRun) LogSize() int {
	
}

// Get log of given name. If there is no such log then nil, false is returned
func (logrun *LogRun) Log(logname string) (Log, bool) {
	log, found := logrun.continousLogs[logname]
	if !found {
		return log, found
	}
	return logrun.discreteLogs[logrun]
}

func (logrun *LogRun) RemoveLogs(logname string) {
	delete(logrun.discreteLogs, logname)
}

func (logrun *LogRun) ResizeLogs(size int) {
	
}